import config

# Запрос на создание таблицы с информацией об агенстве
create_agency_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.agency]} (
									id VARCHAR(10) PRIMARY KEY,
									name VARCHAR(10),
									url VARCHAR(30),
									phone VARCHAR(20),
									timezone VARCHAR(20)
									); """

# Запрос на создание таблицы с информацией об операторе
create_operators_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.operators]} (
									id INTEGER PRIMARY KEY,
									name VARCHAR(50),
									phone VARCHAR(20),
									address VARCHAR(255)
									); """

# Запрос на создание таблицы с информацией о типе маршрута
create_route_types_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.route_types]} (
									id INTEGER PRIMARY KEY,
									name VARCHAR(150)
									); """

# Запрос на создание таблицы с информацией о типе транспорта
create_transport_types_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.transport_types]} (
									id INTEGER PRIMARY KEY,
									name VARCHAR(10)
									); """

# Запрос на создание таблицы с информацией о типах исключений (для таблицы service_dates)
create_exception_types_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.exception_types]} (
									id INTEGER PRIMARY KEY,
									name VARCHAR(50)
									); """

# Запрос на создание таблицы с информацией о графике работы сервиса
create_services_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.services]} (
									id INTEGER PRIMARY KEY,
									name VARCHAR(50),
									monday VARCHAR(1),
									tuesday VARCHAR(1),
									wednesday VARCHAR(1),
									thursday VARCHAR(1),
									friday VARCHAR(1),
									saturday VARCHAR(1),
									sunday VARCHAR(1),
									start_date VARCHAR(20),
									end_date VARCHAR(20)
									); """

# Запрос на создание таблицы с информацией об исключениях для графика (таблица service)
create_service_dates_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.service_dates]} (
									record_id INTEGER PRIMARY KEY,
									service_id INTEGER,
									date VARCHAR(20),
									exception_type_id INTEGER,
									FOREIGN KEY (exception_type_id) REFERENCES {config.tables[config.exception_types]} (id)
									); """

# Запрос на создание таблицы с информацией о рейсах, которые выполняются через регулярный интервал
create_frequency_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.frequency]} (
									trip_id INTEGER PRIMARY KEY,
									start_time VARCHAR(10),
									end_time VARCHAR(10),
									headway_secs INTEGER,
									exact_times VARCHAR(53)
									); """


# Запрос на создание таблицы с маршрутами
create_routes_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.routes]} (
                                    id INTEGER PRIMARY KEY,
									agency_id VARCHAR(10),
									operator_id INTEGER,
									fare_id INTEGER,
									short_name VARCHAR(10),
									long_name VARCHAR(255),
									route_type_id INTEGER,
									transport_type_id INTEGER,
									FOREIGN KEY (agency_id) REFERENCES {config.tables[config.agency]} (id),
									FOREIGN KEY (operator_id) REFERENCES {config.tables[config.operators]} (id),
									FOREIGN KEY (fare_id) REFERENCES {config.tables[config.fares]} (id),
									FOREIGN KEY (route_type_id) REFERENCES {config.tables[config.route_types]} (id),
									FOREIGN KEY (transport_type_id) REFERENCES {config.tables[config.transport_types]} (id)
                                    ); """

# Запрос на создание таблицы с рейсами
create_trips_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.trips]} (
									id INTEGER PRIMARY KEY,
									route_id INTEGER,
									service_id INTEGER,
									shape_id VARCHAR(15),
									direction VARCHAR(7),
									FOREIGN KEY (route_id) REFERENCES {config.tables[config.routes]} (id),
									FOREIGN KEY (service_id) REFERENCES {config.tables[config.services]} (id)
									); """

# Запрос на создание таблицы с остановками
create_stops_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.stops]} (
									id INTEGER,
									stop_info_id INTEGER,
									arrival_time VARCHAR(20),
									departure_time VARCHAR(20),
									stop_sequence INTEGER,
									shape_id VARCHAR(15),
									shape_dist_traveled REAL,
									trip_id INTEGER,
									PRIMARY KEY (id, stop_sequence, trip_id)
									); """

# Запрос на создание таблицы с описанием траекторий движения ТС
create_shapes_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.shapes]} (
									id VARCHAR(13),
									lat REAL,
									lon REAL,
									sequence INTEGER,
									dist_traveled REAL,
									geometry GEOMETRY,
									PRIMARY KEY (id, sequence)
									); """

# Запрос на создание таблицы с информацией о билетах
create_fares_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.fares]} (
									id INTEGER PRIMARY KEY,
									price INTEGER,
									currency_type VARCHAR(10),
									payment_method VARCHAR(31),
									transfers VARCHAR(36)
									); """

# Запрос на создание таблицы с информацией о прибытии ТС на остановку
create_stopforecast_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.stopforecast]} (
									route_id INTEGER,
									stop_id INTEGER,
									vehicle_id INTEGER,
									label VARCHAR(10),
									license_plate VARCHAR(9),
									arrival VARCHAR(20),
									PRIMARY KEY (route_id, stop_id, vehicle_id) 
									); """

# Запрос на создание таблицы с информацией о типах остановок
create_location_types_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.location_types]} (
									id INTEGER PRIMARY KEY,
									name VARCHAR(23)
									); """

# Запрос на создание таблицы с информацией о типах посадок на рейс
create_wheelchair_boarding_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.wheelchair_boarding]} (
									id INTEGER PRIMARY KEY,
									name VARCHAR(79)
									); """

# Запрос на создание таблицы с информацией об остановках
create_stops_info_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.stops_info]} (
									id INTEGER PRIMARY KEY,
									code INTEGER,
									name VARCHAR(255),
									lat REAL,
									lon REAL,
									location_type_id INTEGER,
									wheelchair_boarding_id INTEGER,
									transport_type_id INTEGER,
									geometry GEOMETRY,
									FOREIGN KEY (location_type_id) REFERENCES {config.tables[config.location_types]} (id),
									FOREIGN KEY (wheelchair_boarding_id) REFERENCES {config.tables[config.wheelchair_boarding]} (id)
									
									); """

# Запрос на создание таблицы с информацией о ТС
create_vehicle_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.vehicle]} (
									id INTEGER PRIMARY KEY,
									label VARCHAR(10),
									license_plate VARCHAR(9),
									lat REAL,
									lon REAL,
									bearing REAL,
									speed VARCHAR(10),
									date VARCHAR(20),
									route_id INTEGER,
									geometry GEOMETRY,
									FOREIGN KEY (route_id) REFERENCES {config.tables[config.routes]} (id)
									); """

# Запрос на создание таблицы vehicletrips
create_vehicletrips_table = f""" CREATE TABLE IF NOT EXISTS {config.tables[config.vehicletrips]} (
									vehicle_id INTEGER PRIMARY KEY,
									vehicle_label VARCHAR(10),
									vehicle_license_plate VARCHAR(9),
									route_id INTEGER,
									stop_time_update JSON
									); """

# Создание индексов
create_shapes_geometry_index = f""" CREATE INDEX IF NOT EXISTS {config.index_for_shapes} ON {config.tables[config.shapes]}(geometry); """
create_stops_info_geometry_index = f""" CREATE INDEX IF NOT EXISTS {config.index_for_stops_info} ON {config.tables[config.stops_info]}(geometry); """
create_vehicle_geometry_index = f""" CREATE INDEX IF NOT EXISTS {config.index_for_vehicle} ON {config.tables[config.vehicle]}(geometry); """

# Запрос на добавление или обновление строк в таблице route_type
insert_or_update_route_type = f""" INSERT INTO {config.tables[config.route_types]} VALUES(%s, %s) 
						           ON CONFLICT(id) DO UPDATE SET name=EXCLUDED.name; """

# Запрос на добавление или обновление строк в таблице transport_type
insert_or_update_transport_type = f""" INSERT INTO {config.tables[config.transport_types]} VALUES(%s, %s)
							           ON CONFLICT(id) DO UPDATE SET name=EXCLUDED.name; """

# Запрос на добавление или обновление строк в таблице exception_types
insert_or_update_exception_types = f""" INSERT INTO {config.tables[config.exception_types]} VALUES(%s, %s)
										ON CONFLICT(id) DO UPDATE SET name=EXCLUDED.name; """

# Запрос на добавление или обновление строк в таблице agency
insert_or_update_agency = f""" INSERT INTO {config.tables[config.agency]} VALUES(%s, %s, %s, %s, %s)
							   ON CONFLICT(id) DO UPDATE SET name=EXCLUDED.name, url=EXCLUDED.url,
							   phone=EXCLUDED.phone, timezone=EXCLUDED.timezone; """

# Запрос на добавление или обновление строк в таблице operators
insert_or_update_operators = f""" INSERT INTO {config.tables[config.operators]} VALUES(%s, %s, %s, %s)
								  ON CONFLICT(id) DO UPDATE SET name=EXCLUDED.name, phone=EXCLUDED.phone,
								  address=EXCLUDED.address; """

# Запрос на добавление или обновление строк в таблице fares
insert_or_update_fares = f""" INSERT INTO {config.tables[config.fares]} VALUES(%s, %s, %s, %s, %s)
							  ON CONFLICT(id) DO UPDATE SET price=EXCLUDED.price, currency_type=EXCLUDED.currency_type,
							  payment_method=EXCLUDED.payment_method, transfers=EXCLUDED.transfers; """
# Запрос на добавление или обновление строк в таблице routes
insert_or_update_routes_records = lambda temp_table: f""" INSERT INTO {config.tables[config.routes]} SELECT * FROM {temp_table}
					 ON CONFLICT(id) DO UPDATE SET agency_id=EXCLUDED.agency_id, operator_id=EXCLUDED.operator_id,
					 fare_id=EXCLUDED.fare_id, short_name=EXCLUDED.short_name, long_name=EXCLUDED.long_name,
					 route_type_id=EXCLUDED.route_type_id, transport_type_id=EXCLUDED.transport_type_id; """

# Запрос на добавление или обновление строк в таблице trips
insert_or_update_trips_records = lambda temp_table: f""" INSERT INTO {config.tables[config.trips]} SELECT * FROM {temp_table}
					 ON CONFLICT(id) DO UPDATE SET route_id=EXCLUDED.route_id,
					 service_id=EXCLUDED.service_id, shape_id=EXCLUDED.shape_id, direction=EXCLUDED.direction; """

# Запрос на добавление или обновление строк в таблице stops
insert_or_update_stops_records = lambda temp_table: f""" INSERT INTO {config.tables[config.stops]} SELECT * FROM {temp_table}
					 ON CONFLICT(id, stop_sequence, trip_id) DO UPDATE SET 
					 stop_info_id=COALESCE(EXCLUDED.stop_info_id, {config.tables[config.stops]}.stop_info_id),
					 arrival_time=EXCLUDED.arrival_time, departure_time=EXCLUDED.departure_time,
					 shape_id=EXCLUDED.shape_id, shape_dist_traveled=EXCLUDED.shape_dist_traveled; """

# Запрос на добавление или обновление строк в таблице shapes
insert_or_update_shapes_records = f""" INSERT INTO {config.tables[config.shapes]} 
					 VALUES(%s, %s, %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326))
					 ON CONFLICT(id, sequence) DO UPDATE SET lat=EXCLUDED.lat,
					 lon=EXCLUDED.lon, dist_traveled=EXCLUDED.dist_traveled,
					 geometry=EXCLUDED.geometry; """

# Запрос на добавление или обновление строк в таблице stops_info
insert_or_update_stops_info = f""" INSERT INTO {config.tables[config.stops_info]}
					 VALUES(%s, %s, %s, %s, %s, %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326))
					 ON CONFLICT(id) DO UPDATE SET code=EXCLUDED.code, name=EXCLUDED.name,
					 lat=EXCLUDED.lat, lon=EXCLUDED.lon, location_type_id=EXCLUDED.location_type_id,
					 wheelchair_boarding_id=EXCLUDED.wheelchair_boarding_id, 
					 transport_type_id=EXCLUDED.transport_type_id, geometry=EXCLUDED.geometry; """

# Анонимная функция, возвращающая запрос на добавление или обновление строк в таблице services
insert_or_update_services_table = lambda temp_table: f""" INSERT INTO {config.tables[config.services]} SELECT * FROM {temp_table}
					 ON CONFLICT(id) DO UPDATE SET name=EXCLUDED.name, monday=EXCLUDED.monday,
					 tuesday=EXCLUDED.tuesday, wednesday=EXCLUDED.wednesday, thursday=EXCLUDED.thursday, 
					 friday=EXCLUDED.friday, saturday=EXCLUDED.friday, sunday=EXCLUDED.sunday, 
					 start_date=EXCLUDED.start_date, end_date=EXCLUDED.end_date; """

# Анонимная функция, возвращающая запрос на добавление или обновление строк в таблице service_dates
insert_or_update_service_dates_table = lambda temp_table: f""" INSERT INTO {config.tables[config.service_dates]} SELECT * FROM {temp_table}
					 ON CONFLICT(record_id) DO UPDATE SET service_id=EXCLUDED.service_id, date=EXCLUDED.date,
					 exception_type_id=EXCLUDED.exception_type_id; """

# Анонимная функция, возвращающая запрос на добавление или обновление строк в таблице frequency
insert_or_update_frequency = lambda temp_table: f""" INSERT INTO {config.tables[config.frequency]} SELECT * FROM {temp_table}
					 ON CONFLICT(trip_id) DO UPDATE SET start_time=EXCLUDED.start_time, end_time=EXCLUDED.end_time,
					 headway_secs=EXCLUDED.headway_secs, exact_times=EXCLUDED.exact_times; """

# Запрос на добавление или обновление строк в таблице location_types
insert_or_update_location_types = f""" INSERT INTO {config.tables[config.location_types]} VALUES(%s, %s)
									   ON CONFLICT(id) DO UPDATE SET name=EXCLUDED.name; """

# Запрос на добавление или обновление строк в таблице wheelchair_boarding
insert_or_update_wheelchair_boarding = f""" INSERT INTO {config.tables[config.wheelchair_boarding]} VALUES(%s, %s)
									   ON CONFLICT(id) DO UPDATE SET name=EXCLUDED.name; """

# Запрос на добавление строк в таблицу vehicle
insert_vehicle = f""" INSERT INTO {config.tables[config.vehicle]} 
								VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326)); """

# Запрос на очищение таблицы vehicle
truncate_vehicle = f""" TRUNCATE {config.tables[config.vehicle]}; """

# Запрос на добавление строк в таблицу stopforecast
insert_stopforecast = f""" INSERT INTO {config.tables[config.stopforecast]} VALUES(%s, %s, %s, %s, %s, %s); """

# Запрос на очищение таблицы stopforecast
truncate_stopforecast = f""" TRUNCATE {config.tables[config.stopforecast]}; """

# Запрос на добавление таблицу vehicletrips
insert_vehicletrips = f""" INSERT INTO {config.tables[config.vehicletrips]} VALUES(%s, %s, %s, %s, %s); """

# Запрос на очищение таблицы vehicletrips
truncate_vehicletrips = f""" TRUNCATE {config.tables[config.vehicletrips]}; """

# Запрос на обновление данных о ТС в таблице routes
update_routes_table = f""" UPDATE {config.tables[config.routes]} SET vehicle = %s WHERE id = %s; """

# Запрос на обновление информации о прибытии ТС на конкретную остановку
update_arrival_query = f""" UPDATE {config.tables[config.stops]} SET vehicles = %s
													WHERE id = %s AND trip_id = %s; """

# Получение рейсов, которые следуют по маршруту, имеющему соответствующий идентификатор
select_trips_query = f""" SELECT id FROM {config.tables[config.trips]} WHERE route_id = %s; """

# Запрос на копирование csv-файла в таблицу БД
copy_csv_in_table = lambda table_name: f""" COPY {table_name} FROM STDIN DELIMITER ',' CSV HEADER; """

# Запрос на создание временной таблицы БД
create_temp_table = lambda temp_table_name, base_table_name: f""" CREATE TEMPORARY TABLE {temp_table_name} (
																						 like {base_table_name}
																						 including defaults
																						 including constraints
																						 including indexes
																						 ); """

# Запрос на удаление временной таблицы БД
drop_temp_table = lambda temp_table: f""" DROP TABLE {temp_table}; """

# Запрос на выдачу id всех ТС из таблицы vehicle
get_all_vehicle_ids = f""" SELECT id FROM {config.tables[config.vehicle]}; """

# Словарь соответствий между названиями таблиц и запросами для их заполнения
table_query = {
    config.tables[config.routes]: lambda temp_table: insert_or_update_routes_records(temp_table),
    config.tables[config.trips]: lambda temp_table: insert_or_update_trips_records(temp_table),
    config.tables[config.stops]: lambda temp_table: insert_or_update_stops_records(temp_table),
    config.tables[config.services]: lambda temp_table: insert_or_update_services_table(temp_table),
    config.tables[config.service_dates]: lambda temp_table: insert_or_update_service_dates_table(temp_table),
    config.tables[config.frequency]: lambda temp_table: insert_or_update_frequency(temp_table),
    config.tables[config.agency]: insert_or_update_agency,
    config.tables[config.operators]: insert_or_update_operators,
    config.tables[config.fares]: insert_or_update_fares,
    config.tables[config.route_types]: insert_or_update_route_type,
    config.tables[config.transport_types]: insert_or_update_transport_type,
    config.tables[config.exception_types]: insert_or_update_exception_types,
    config.tables[config.shapes]: insert_or_update_shapes_records,
    config.tables[config.stops_info]: insert_or_update_stops_info,
    config.tables[config.location_types]: insert_or_update_location_types,
    config.tables[config.wheelchair_boarding]: insert_or_update_wheelchair_boarding,
    config.tables[config.vehicle]: insert_vehicle,
    config.tables[config.stopforecast]: insert_stopforecast,
    config.tables[config.vehicletrips]: insert_vehicletrips
}


