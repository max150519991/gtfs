from os import getenv

# Константы для соединения с БД
HOST = getenv('POSTGRES_HOST', 'postgres') # Хост БД
USERNAME = getenv('POSTGRES_USER', 'starlinemaps') # Пользователь БД
PASSWORD = getenv('POSTGRES_PASSWORD', 'starlinemaps') # Пароль БД
DB_NAME = getenv('POSTGRES_DB', 'gtfs') # Название БД
DB_PORT = getenv('POSTGRES_PORT', 5432) # Порт БД
DB_PREFIX = getenv('TRANSIT_DB_PREFIX', '') # Префикс перед названием таблиц БД

# Ключи названий таблиц в словаре tables
routes = 'routes'; trips = 'trips'; stops = 'stops'; shapes = 'shapes'; agency = 'agency'; operators = 'operators'
fares = 'fares'; route_types = 'route_types'; transport_types = 'transport_types'; location_types = 'location_types'
exception_types = 'exception_types'; wheelchair_boarding = 'wheelchair_boarding'; stops_info = 'stops_info'
frequency = 'frequency'; services = 'services'; service_dates = 'service_dates'; vehicle = 'vehicle'
stopforecast = 'stopforecast'; vehicletrips = 'vehicletrips'

# Индексы для полей geometry (таблицы: shapes, stops_info и vehicle)
index_for_shapes = 'shapes_index'
index_for_stops_info = 'stops_info_index'
index_for_vehicle = 'vehicle_index'

# Название таблиц БД
tables = {
     routes: 'routes', trips: 'trips', stops: 'stops', shapes: 'shapes', agency: 'agency', operators: 'operators',
     fares: 'fares', route_types: 'route_types', transport_types: 'transport_types', location_types: 'location_types',
     exception_types: 'exception_types', wheelchair_boarding: 'wheelchair_boarding', stops_info: 'stops_info',
     frequency: 'frequency', services: 'services', service_dates: 'service_dates', vehicle: 'vehicle',
     stopforecast: 'stopforecast', vehicletrips: 'vehicletrips'
}

# Если префикс не пустой, добавить его перед названием таблиц БД и индексов таблиц
if DB_PREFIX:
    for key in tables.keys():
        tables[key] = DB_PREFIX + '_' + tables[key]

    index_for_shapes = DB_PREFIX + '_' + index_for_shapes
    index_for_stops_info = DB_PREFIX + '_' + index_for_stops_info
    index_for_vehicle = DB_PREFIX + '_' + index_for_vehicle

# Интервал, через который архив с данными повторно загружается с сайта
SECONDS_IN_DAY =  86_400

# Ссылка на главную страницу сайта
BASE_URL = 'https://transport.orgp.spb.ru/Portal/transport/internalapi/gtfs/'

# Константы, необходимые для работы скрипта
HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/79.0.3945.88 Safari/537.36'
}  # Заголовки

ARCHIVE_URL = BASE_URL + 'feed.zip' # Ссылка для загрузки архива

# Ссылки для загрузки файлов с данными в реальном времени
FILES_URLS = [
    'realtime/vehicle',
    'realtime/vehicletrips?vehicleIDs=',
    'realtime/stopforecast?stopID=1791'
]