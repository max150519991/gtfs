from typing import Any

from google.transit import gtfs_realtime_pb2  # Библиотека для чтения бинарных файлов с данными в реальном времени
from google.protobuf.json_format import MessageToDict  # Библиотека для чтения структуры бинаного в виде словаря
import requests  # Библиотека для работы с сетью
import zipfile  # Библиотека для работы с zip-файлами
import csv  # Библиотека для работы с csv-файлами
import json  # Библиотека для работы с json-файлами
import logging  # Библиотека для логгирования
from database import DB  # Класс базы данных
from datetime import datetime  # Функция для работы с датой
from time import time, sleep  # Функции для работы с временем
import public_transportation_data  # Модуль со словарями соответствий
import config  # Модуль с настойками подключения к БД и константами
import queries  # Модуль с запросами к БД
import os  # Модуль для работы с ОС
from shutil import rmtree  # Функция для удаления директории с файлами

# Установка параметров логгирования
logging.basicConfig(
	format='%(asctime)s %(levelname)-8s %(name)s - %(message)s',
	datefmt='%Y-%m-%d %H:%M:%S'
)

logging.getLogger('urllib3.connectionpool').setLevel(logging.WARNING)

logger = logging.getLogger('GTFS_parser') # Название логгера
logger.setLevel(logging.INFO) # Установка уровня логгирования - INFO

archive_download_ts = None  # Дата последней загрузки архива с сайта
update_files_dict = {}  # Словарь с именами файлов и датами их обновлений
update_realtime = True # Булева переменная, показывающая нужно ли обновлять realtime-данные

def initialization_db() -> DB:
	""" Функция для инициализации объекта БД. Для инициализации используются константы из модуля config

	  return db: Объект БД
	"""

	# Создание экземпляра класса DB
	db = DB(
		host=config.HOST,
		# host='127.0.0.1',
		port=config.DB_PORT,
		# port=5432,
		user=config.USERNAME,
		# user='postgres',
		password=config.PASSWORD,
		# password='postgreSQL123',
		db_name=config.DB_NAME
		# db_name='gtfs'
	)

	db.connection_with_db()  # Установление соединения с БД

	return db # Вернуть объект БД

db = initialization_db()

def insert_rows_in_db(table_name: str, data):
	""" Функция для вставки одной строки в таблицу БД

	 :param table_name: Название таблицы БД
	 :param data: Список либо Кортеж с данными для вставки
	 """

	is_ext = False # Булева переменная, отвечающая за множественную вставку данных
	query = queries.table_query[table_name] # Запрос на вставку данных для конкретной таблицы

	if table_name in (
						config.tables[config.shapes], config.tables[config.stops_info],
					    config.tables[config.vehicle], config.tables[config.stopforecast],
					    config.tables[config.vehicletrips]
					):

		# Очищение соответствующей таблицы с помощью TRUNCATE
		if table_name == config.tables[config.vehicle]:
			db.query_execute(queries.truncate_vehicle)
		elif table_name == config.tables[config.stopforecast]:
			db.query_execute(queries.truncate_stopforecast)
		elif table_name == config.tables[config.vehicletrips]:
			db.query_execute(queries.truncate_vehicletrips)

		is_ext = True

	db.query_execute(query=query, params=data, ext=is_ext)

def create_csv_file_and_copy_in_db(headers: list, data: list, table_name: str):
	""" Функция, создающая csv-файл с данными и сохраняющая его в таблицу БД

	 :param headers: Названия столбцов в csv-файле
	 :param data: Список с кортежами данных
	 :param table_name: Название таблицы БД, в которую необходимо скопировать csv-файл
	 """

	csv_name = table_name + '.csv'  # Название csv-файла

	# Запись данных в csv-файл
	with open(file=csv_name, mode='a', encoding='UTF-8') as ff:
		wr = csv.writer(ff)
		wr.writerow(headers)
		for line in data:
			wr.writerow(line)

	# Создание временной таблицы
	temp_table = table_name + '_temp'
	create_temp_table = queries.create_temp_table(temp_table_name=temp_table, base_table_name=table_name)
	db.query_execute(create_temp_table)

	db.copy_file(query=queries.copy_csv_in_table(table_name=temp_table), filename=csv_name)

	logger.info(f'Заполнение таблицы {table_name}')

	# Заполнение или обновление соответствующей таблицы с помощью временной
	query = queries.table_query[table_name](temp_table)
	db.query_execute(query)

	# Удаление временной таблицы
	db.query_execute(queries.drop_temp_table(temp_table))

	# Удаление csv-файла
	os.remove(csv_name)


def download_file(url: str, name: str) -> bool:
	""" Функция для скачивания файлов с интернет-ресурса

	 :param url: Ссылка на интернет-ресурс
	 :param name: Название архива файла
	 :return: Булево значение, сообщающее об успешной загрузке файла или о неудаче
	 """

	response = requests.get(url=url, headers=config.HEADERS)

	if response.status_code == 200:
		with open(file=name, mode='wb') as f:
			f.write(response.content)
			return True
	return False

def download_file_with_print_result(file_name: str, file_url: str, downloaded_files: list=None):
	""" Функция для загрузки файла с последующим выводом статистики

	 :param file_name: Название файла
	 :param file_url: Ссылка для загрузки файла
	 :param downloaded_files: Список загруженных файлов. По умолчанию None.
	 Если передать список, функция сохранит название загруженного файла в данный список
	 """

	# Попытка загрузки файла с realtime-данными
	try:
		res = download_file(url=file_url, name=file_name)
	except Exception as e:
		logger.error(f'При загрузке файла с realtime-данными возникло исключение: {e}')
	else:
		if res:
			logger.debug(f'Файл {file_name} успешно загружен!')
			downloaded_files.append(file_name)
		else:
			logger.error(f'Ошибка при загрузке файла {file_name}')


def unpacking_zip(zip_name: str, dir_to_unpack: str) -> list:
	""" Функция, распаковывающая zip-архив с данными, скачанный с интернет-ресурса

	 :param zip_name: Название архива
	 :param dir_to_unpack: Директория, в которую будет распакован архив
	 :return unpacking_files: Список распакованных файлов без расширений
	 """

	logger.debug(f'Распаковка архива {zip_name}')

	# Создание директории, в которую будут распакованы файлы архива
	if os.path.exists(dir_to_unpack):
		os.mkdir(dir_to_unpack)

	unpacking_files = [] # Список распакованных файлов

	# Распаковка архива
	with zipfile.ZipFile(file=zip_name) as zip_:
		for file in zip_.infolist():
			file_update_time = file.date_time

			# Если дата обновления файла не соответствует дате обновления, записанной в словаре,
			# распаковать данный файл и занести дату его обновления в словарь
			if update_files_dict.get(file) != file_update_time:
				update_files_dict[file] = file_update_time
				zip_.extract(file, path=dir_to_unpack)
				file_name_without_extension = file.filename.split('.')[0] # Название файла без расширения
				unpacking_files.append(file_name_without_extension)

	os.remove(zip_name)
	logger.debug(f'Архив {zip_name} успешно распакован')
	return unpacking_files


def get_date_from_timestamp(timestamp: int) -> datetime.fromtimestamp:
	""" Функция для получения даты из timestamp

	 :param timestamp: Количество секунд, прошедшее с начала эпохи
	 :return date: Дата, полученная из timestamp
	 """

	date = datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
	return date


def read_file_decorator(filename: str):
	""" Декоратор, который оборачивает каждую функцию парсинга файла.
		Декоратор читает строки из файла, передаёт их функции для парсинга и
		при необходимости сохраняет полученные данные в таблицу БД

	    :param filename: Название файла
		"""

	def read_file(func):
		def wrapper(*args, **kwargs):
			if os.path.exists(filename):
				# Открытие файла для чтения
				with open(file=filename, mode='r', encoding='UTF-8') as csvfile:
					table = kwargs.get('table') # Название таблицы БД для сохранения данных
					lines = csv.reader(csvfile, delimiter=',')  # Считывание строк
					# (строка с названиями столбцов)

					headers = next(lines)  # Названия столбцов в csv-файле
					res = func(args[0], lines=lines)  # Остальные строки с информацией

					# Если название таблицы передано в функцию
					if table is not None:
						is_create_csv = kwargs.get('is_create_csv')

						if is_create_csv is not None:
							for data in res:
								insert_rows_in_db(table_name=table, data=data)
						else:
							create_csv_file_and_copy_in_db(headers=headers, data=res, table_name=table)
					else:  # Иначе вернуть результат работы функции чтения строк из файла
						return res
			return None
		return wrapper
	return read_file


class GTFSStaticParser:
	""" Класс, содержащий методы для парсинга текстовых файлов из архива """

	DIR_WITH_FILES = 'feed' # Название директории, в которой находятся файлы для парсинга

	# Название файлов из архива, которые необходимо спарсить
	FILES_TO_PARSE = {
		'calendar': f'{DIR_WITH_FILES}/calendar.txt',
		'calendar_dates': f'{DIR_WITH_FILES}/calendar_dates.txt',
		'trips': f'{DIR_WITH_FILES}/trips.txt',
		'frequencies': f'{DIR_WITH_FILES}/frequencies.txt',
		'operators': f'{DIR_WITH_FILES}/operators.txt',
		'fare_attributes': f'{DIR_WITH_FILES}/fare_attributes.txt',
		'agency': f'{DIR_WITH_FILES}/agency.txt',
		'routes': f'{DIR_WITH_FILES}/routes.txt',
		'stops': f'{DIR_WITH_FILES}/stops.txt',
		'shapes': f'{DIR_WITH_FILES}/shapes.txt',
		'stop_times': f'{DIR_WITH_FILES}/stop_times.txt',
		'operator_routes': f'{DIR_WITH_FILES}/operator_routes.txt',
		'fare_rules': f'{DIR_WITH_FILES}/fare_rules.txt',
	}

	def __init__(self):
		self.transport_types_dict = {}

	@read_file_decorator(FILES_TO_PARSE['agency'])
	def parse_agency(self, lines=None):
		""" Функция для парсинга файла agency.txt """

		if lines:
			for line in lines:
				id_ = line[0]  # Идентификатор агенства
				name = line[1].encode('latin1').decode('utf8', 'replace')  # Название агенства
				url = line[2]  # URL агенства
				phone = line[3]  # Телефон агенства
				timezone = line[4]  # Временная зона

				yield id_, name, url, phone, timezone
		else:
			logger.debug('В файле нет записей')

	@read_file_decorator(FILES_TO_PARSE['operators'])
	def parse_operators(self, lines=None):
		""" Функция для парсинга файла operators.txt """

		empty_data = (-1, 'Нет данных', 'Нет данных', 'Нет данных')
		if lines:
			for line in lines:
				id_ = int(line[0])  # Идентификатор оператора
				name = line[1]  # Название оператора
				phone = line[2]  # Телефон оператора
				address = line[3]  # Адресс оператора

				yield id_, name, phone, address
			yield empty_data
		else:
			logger.debug('В файле нет записей')

	@read_file_decorator(FILES_TO_PARSE['fare_attributes'])
	def parse_fares(self, lines=None):
		""" Функция для парсинга файла fare_attributes.txt """

		empty_data = (-1, 0, 'Нет данных', 'Нет данных', 'Нет данных')
		if lines:
			for line in lines:
				id_ = int(line[0])  # Идентификатор билета
				price = int(line[1])  # Цена за билет
				currency_type = line[2]  # Валюта (RUB)
				payment_method = public_transportation_data.payment_method.get(line[3])  # Способ оплаты
				transfers = public_transportation_data.transfers.get(line[4])  # Возможность пересадки

				yield id_, price, currency_type, payment_method, transfers
			yield empty_data

		else:
			logger.debug('В файле нет записей')

	@read_file_decorator(FILES_TO_PARSE['routes'])
	def parse_routes(self, lines=None):
		""" Метод для парсинга файла с путями routes.txt """

		if lines:
			routes_operators_dict = self.__get_routes_operators_dict()
			routes_fares_dict = self.__get_routes_fares_dict()
			set_routes_types_id = set()
			set_transport_types = set()
			transport_type_id = 0

			for line in lines:
				route_id = int(line[0])
				agency_id = line[1]
				operator_id = routes_operators_dict.get(route_id, -1)
				fare_id = routes_fares_dict.get(route_id, -1)
				route_short_name = line[2]
				route_long_name = line[3]
				route_type_id = int(line[4])
				transport_type = line[5]

				if route_type_id not in set_routes_types_id:
					set_routes_types_id.add(route_type_id)
					route_type_text = public_transportation_data.route_type.get(route_type_id, '')
					insert_rows_in_db(table_name=config.tables[config.route_types], data=(route_type_id, route_type_text))

				if transport_type not in set_transport_types:
					transport_type_id += 1
					set_transport_types.add(transport_type)
					insert_rows_in_db(table_name=config.tables[config.transport_types],
									 data=(transport_type_id, transport_type))

				yield (
					route_id, agency_id, operator_id, fare_id, route_short_name,
					route_long_name, route_type_id, transport_type_id
				)
		else:
			logger.debug('В файле нет записей')

	@read_file_decorator(FILES_TO_PARSE['trips'])
	def parse_trips(self, lines=None):
		""" Метод для парсинга файла с рейсами trips.txt """

		if lines:
			for line in lines:
				trip_id = int(line[2])  # Идентификатор рейса
				route_id = int(line[0])  # Идентификатор маршрута
				service_id = int(line[1])  # Идентификатор сервиса
				direction = public_transportation_data.trip_direction_id.get(line[3])  # Направление рейса
				shape_id = line[4]  # Идентификатор траектории

				yield trip_id, route_id, service_id, shape_id, direction
		else:
			logger.debug('В файле нет записей')

	@read_file_decorator(FILES_TO_PARSE['shapes'])
	def parse_shapes(self, lines=None):
		""" Функция для парсинга файла с траекториями движения ТС """

		shapes_data = []

		if lines:
			for i, line in enumerate(lines, 1):
				id_ = line[0]  # Уникальный идентификатор траектории движения ТС
				lat, lon = float(line[1]), float(line[2])  # Широта и долгота точки в траектории движения ТС
				sequence = int(line[3])  # Точка последовательности траектории движения ТС
				dist_traveled = float(line[4])  # Дистанция от начальной точки последовательности до данной

				shapes_data.append((id_, lat, lon, sequence, dist_traveled, lon, lat))

				if i % 1000 == 0:
					yield shapes_data
					shapes_data.clear()

		if shapes_data:
			yield shapes_data

	@read_file_decorator(FILES_TO_PARSE['stops'])
	def parse_stops(self, lines=None):
		""" Функция для парсинга файла с остановками """

		stops_data = []

		if lines:
			set_location_types = set()
			set_wheelchair_boardings = set()
			for i, line in enumerate(lines, 1):
				id_ = int(line[0])  # Уникальный идентификатор остановки
				code = int(line[1])  # Код остановки
				name = line[2]  # Название остановки
				lat, lon = float(line[3]), float(line[4]) # Широта и долгота остановки
				location_type_id = int(line[5])
				wheelchair_boarding_id = int(line[6])

				if location_type_id not in set_location_types:
					set_location_types.add(location_type_id)
					location_type_desc = public_transportation_data.location.get(location_type_id)
					insert_rows_in_db(table_name=config.tables[config.location_types],
									  data=(location_type_id, location_type_desc))

				if wheelchair_boarding_id not in set_wheelchair_boardings:
					set_wheelchair_boardings.add(wheelchair_boarding_id)
					wheelchair_boarding_desc = public_transportation_data.wheelchair_boarding.get(wheelchair_boarding_id)
					insert_rows_in_db(table_name=config.tables[config.wheelchair_boarding],
									  data=(wheelchair_boarding_id, wheelchair_boarding_desc))

				transport_type_id = self.transport_types_dict.get(line[7], 1)

				stops_data.append((id_, code, name, lat, lon, location_type_id,
								   wheelchair_boarding_id, transport_type_id, lon, lat))

				if i % 1000 == 0:
					yield stops_data
					stops_data.clear()

			if stops_data:
				yield stops_data

	@read_file_decorator(FILES_TO_PARSE['stop_times'])
	def parse_stop_times(self, lines=None):
		""" Метод для парсинга файла с порядками остановок """

		stops_times_data = []
		if lines:
			for i, line in enumerate(lines, 1):
				trip_id = int(line[0])  # Идентификатор рейса
				arrival_time = line[1]  # Время прибытия
				departure_time = line[2]  # Время отбытия
				stop_id = int(line[3])  # Идентификатор остановки
				stop_sequence = int(line[4])
				shape_id = line[5]
				dist_traveled = float(line[6])

				yield (stop_id, stop_id, arrival_time, departure_time,
					   					 stop_sequence, shape_id, dist_traveled, trip_id)

	@read_file_decorator(FILES_TO_PARSE['operator_routes'])
	def __get_routes_operators_dict(self, lines=None) -> dict:
		""" Метод для получения словаря соответствий идентификаторов маршрутов и идентификаторов операторов

		   :return routes_operators_dict: Словарь соответствий идентификаторов маршрутов и идентификаторов операторов
		 """

		routes_operators_dict = self.__parse_helper_file(lines)
		return routes_operators_dict

	@read_file_decorator(FILES_TO_PARSE['fare_rules'])
	def __get_routes_fares_dict(self, lines=None):
		""" Метод для получения словаря соответствий идентификаторов маршрутов и идентификаторов билетов

		  :return routes_operators_dict: Словарь соответствий идентификаторов маршрутов и идентификаторов билетов
		  """

		routes_fares_dict = self.__parse_helper_file(lines)
		return routes_fares_dict

	@staticmethod
	def __parse_helper_file(lines: list) -> dict:
		""" Статический метод для парсинга информации с соответствиями индексов

		 :param lines: Список строк, полученных из файла
		 :return: Словарь соответствия индексов
		 """

		indexes_dict = {}

		if lines:
			for line in lines:
				first_id = int(line[0])
				second_id = int(line[1])
				indexes_dict[second_id] = first_id
		return indexes_dict

	@read_file_decorator(FILES_TO_PARSE['calendar'])
	def parse_services(self, lines=None):
		""" Функция для парсинга файла calendar.txt """

		if lines:
			for line in lines:
				id_ = int(line[0])  # Идентификатор сервиса
				mon, tue, wed, thu, fri, sat, sun = line[1:8]  # Дни недели работы сервиса
				start_date = get_date_from_timestamp(int(line[8]))  # Дата начала работы сервиса
				end_date = get_date_from_timestamp(int(line[9]))  # Дата окончания работы сервиса

				service_name = line[10]  # Название сервиса

				yield id_, service_name, mon, tue, wed, thu, fri, sat, sun, start_date, end_date
	@read_file_decorator(FILES_TO_PARSE['calendar_dates'])
	def parse_service_dates(self, lines=None):
		if lines:
			set_exception_types = set()
			for i, line in enumerate(lines, 1):
				id_ = int(line[0])  # Идентификатор сервиса
				date = get_date_from_timestamp(int(line[1]))
				exception_type_id = int(line[2]) # Идентификатор типа исключения

				if exception_type_id not in set_exception_types:
					set_exception_types.add(exception_type_id)
					exception_type_text = public_transportation_data.exception_types.get(exception_type_id)
					insert_rows_in_db(table_name=config.tables[config.exception_types],
									  data=(exception_type_id, exception_type_text))

				yield i, id_, date, exception_type_id

	@read_file_decorator(FILES_TO_PARSE['frequencies'])
	def parse_frequencies(self, lines=None):
		""" Функция для парсинга файла с интервалами между рейсами frequencies.txt """

		if lines:
			for line in lines:
				trip_id = int(line[0])  # Идентификатор рейса
				start_time = line[1]  # Время начала отправления рейса
				end_time = line[2]  # Время прибытия рейса
				headway_secs = line[3]  # Интервал
				exact_times = public_transportation_data.exact_times.get(line[4])  # Соответствие расписанию

				yield trip_id, start_time, end_time, headway_secs, exact_times


class GTFSRealtimeParser:
	""" Класс, содержащий методы для парсинга бинарных файлов с данными в реальном времени """

	def define_file(self, filename: str):
		""" Метод, определяющий какой метод необходимо запустить для парсинга бинарного файла

		 :param filename: Название бинарного файла
		 """

		entity = self.__get_entity(filename)

		if entity:
			logger.info(f'Парсинг бинарного файла {filename}')

			if filename == 'stopforecast':
				self.parse_stopforecast(entity)
			elif filename == 'vehicle':
				self.parse_vehicle(entity)
			elif 'vehicletrips' in filename:
				self.parse_vehicletrips(entity)

		else:
			logger.info(f'В бинарном файле {filename} нет данных для парсинга')

		os.remove(filename)

	def parse_stopforecast(self, entity: Any):
		""" Метод для парсинга бинарного файла с информацией об остановках

		  :param entity: Сущность с данными
		"""

		stopforecast_list = []

		for elem in entity:
			trip_update = elem['tripUpdate']  # Информация о рейсе
			vehicle = trip_update.get('vehicle', '')  # Информация о ТС, выполняющем данный рейс
			vehicle_id = int(vehicle.get('id'))  # Идентификатор ТС
			vehicle_label = vehicle.get('label')  # Краткое название ТС
			vehicle_license_plate = vehicle.get('licensePlate')  # Номерной знак ТС

			# Идентификатор маршрута, на котором находится ТС в данный момент времени
			route_id = trip_update.get('trip', '').get('routeId')
			if route_id:
				route_id = int(route_id)
			else:
				route_id = -1
			stop_time_update = trip_update.get('stopTimeUpdate')  # Обновленное время прибытия на остановку
			arrival, stop_id = '', -1  # Время прибытия, идентификатор остановки
			if stop_time_update:
				arrival = stop_time_update[0].get('arrival', '').get('time')  # Получение timestamp времени прибытия
				if arrival: arrival = get_date_from_timestamp(int(arrival))  # Перевод timestamp в дату

				stop_id = int(stop_time_update[0].get('stopId', '-1'))  # Получение идентификатора остановки

			stopforecast_list.append((route_id, stop_id, vehicle_id, vehicle_label,
									  vehicle_license_plate, arrival))

		insert_rows_in_db(table_name=config.tables[config.stopforecast],
							  data=stopforecast_list)

	def parse_vehicle(self, entity: Any):
		""" Метод для парсинга бинарного файла с информацией о ТС

		 :param entity: Сущность с данными
		 """

		vehicles_list = []

		for elem in entity:
			vehicle_info = elem['vehicle']  # Информация о ТС

			# Идентификатор маршрута, на котором находится данное ТС в настоящий момент времени
			route_id = vehicle_info['trip'].get('routeId')
			if route_id: route_id = int(route_id)  # Приведение идентификатора маршрута к числовому типу
			position = vehicle_info['position']  # Получение позиции ТС в данный момент времени

			# Координаты ТС в данный момент времени(широта/долгота)
			lat, lon = float(position['latitude']), float(position['longitude'])

			# Направление в градусах
			bearing = float(position.get('bearing'))

			# Скорость в м/с переведённая в км/ч
			speed = position.get('speed')
			if speed is not None: speed = f'{round(float(speed) * 3600 / 1000, 2)} км/ч'

			vehicle_param = vehicle_info['vehicle']  # Подробная информация о ТС

			vehicle_id = int(vehicle_param.get('id'))  # id транспортного средства
			vehicle_label = vehicle_param.get('label')  # Название ТС
			vehicle_license_plate = vehicle_param.get('licensePlate')  # Номерной знак ТС

			# Время, когда определялось местоположение транспортного средства
			timestamp = vehicle_info.get('timestamp')
			if timestamp:
				timestamp = get_date_from_timestamp(int(timestamp))

			vehicles_list.append((vehicle_id, vehicle_label, vehicle_license_plate, lat, lon,
								  bearing, speed, timestamp, route_id, lon, lat))

		insert_rows_in_db(table_name=config.tables[config.vehicle], data=vehicles_list)

	def parse_vehicletrips(self, entity):
		""" Метод для парсинга бинарного файла с уведомлениями об изменении в расписании

		  :param entity: Сущность с данными
		"""

		vehicletrips_list = []
		for elem in entity:
			stop_time_update_dict = {}

			trip_update = elem['tripUpdate']
			vehicle_info = trip_update['vehicle']
			vehicle_id = vehicle_info['id']
			vehicle_label = vehicle_info['label']
			vehicle_license_plate = vehicle_info['licensePlate']
			route_id = int(trip_update['trip']['routeId'])
			stop_time_update = trip_update.get('stopTimeUpdate')

			json_data = None
			if stop_time_update:
				for stop_time in stop_time_update:
					arrival = stop_time['arrival']
					date = get_date_from_timestamp(int(arrival['time']))
					stop_id = arrival.get('stopId')

					if not stop_id:
						stop_id = stop_time.get('stopId')

					stop_time_update_dict[stop_id] = date
				json_data = json.dumps(stop_time_update_dict, ensure_ascii=False)

			vehicletrips_list.append((vehicle_id, vehicle_label, vehicle_license_plate,
									  route_id, json_data))

		insert_rows_in_db(table_name=config.tables[config.vehicletrips], data=vehicletrips_list)

	@staticmethod
	def __get_entity(filename: str) -> Any:
		""" Метод для получения сущности с данными из бинарного файла

		 :param filename: Название бинарного файла
		 :return entity: Сущность с данными, полученная из файла
		 """

		feed = gtfs_realtime_pb2.FeedMessage()

		# Открытие файла в режиме бинарного чтения
		with open(filename, 'rb') as f:
			feed.ParseFromString(f.read())

		entity = MessageToDict(feed).get('entity')  # Получение из файла сущности с данными
		return entity


def main():
	""" Точка входа """

	global archive_download_ts

	db.query_execute(queries.create_agency_table)
	db.query_execute(queries.create_operators_table)
	db.query_execute(queries.create_fares_table)
	db.query_execute(queries.create_route_types_table)
	db.query_execute(queries.create_transport_types_table)
	db.query_execute(queries.create_services_table)
	db.query_execute(queries.create_exception_types_table)
	db.query_execute(queries.create_service_dates_table)
	db.query_execute(queries.create_frequency_table)
	db.query_execute(queries.create_routes_table)
	db.query_execute(queries.create_trips_table)
	db.query_execute(queries.create_location_types_table)
	db.query_execute(queries.create_wheelchair_boarding_table)
	db.query_execute(queries.create_stops_info_table)
	db.query_execute(queries.create_stops_table)
	db.query_execute(queries.create_shapes_table)
	db.query_execute(queries.create_vehicle_table)
	db.query_execute(queries.create_stopforecast_table)
	db.query_execute(queries.create_vehicletrips_table)

	if archive_download_ts is None or time() - archive_download_ts > config.SECONDS_IN_DAY:
		zip_name = 'feed.zip'  # Название архива
		dir_to_unpack = zip_name.split('.')[0]  # Название директории, в которую будет распакован архив

		logger.info('Скачивание архива с сайта')
		is_downloaded = download_file(url=config.ARCHIVE_URL,
									  name=zip_name)  # True - если удалось загрузить архив, False - если нет

		if is_downloaded:
			# Список распакованных обновленных файлов, которые необходимо спарсить
			unpacking_files = unpacking_zip(zip_name=zip_name, dir_to_unpack=dir_to_unpack)

			# Создание экземпляра класса для парсинга статических файлов
			gtfs_static_parser = GTFSStaticParser()

			if 'agency' in unpacking_files:
				logger.info('Парсинг agency')
				gtfs_static_parser.parse_agency(table=config.tables[config.agency], is_create_csv=False)

			if 'operators' in unpacking_files:
				logger.info('Парсинг operators')
				gtfs_static_parser.parse_operators(table=config.tables[config.operators], is_create_csv=False)

			if 'fare_attributes' in unpacking_files:
				logger.info('Парсинг fares')
				gtfs_static_parser.parse_fares(table=config.tables[config.fares], is_create_csv=False)

			if 'calendar' in unpacking_files:
				logger.info('Парсинг services')
				gtfs_static_parser.parse_services(table=config.tables[config.services])

			if 'calendar_dates' in unpacking_files:
				logger.info('Парсинг service_dates')
				gtfs_static_parser.parse_service_dates(table=config.tables[config.service_dates])

			if 'frequencies' in unpacking_files:
				logger.info('Парсинг frequency')
				gtfs_static_parser.parse_frequencies(table=config.tables[config.frequency])

			if 'routes' in unpacking_files:
				logger.info('Парсинг routes')
				gtfs_static_parser.parse_routes(table=config.tables[config.routes])

			if 'trips' in unpacking_files:
				logger.info('Парсинг trips')
				gtfs_static_parser.parse_trips(table=config.tables[config.trips])

			if 'stops' in unpacking_files:
				logger.info('Парсинг stops_info')
				gtfs_static_parser.parse_stops(table=config.tables[config.stops_info], is_create_csv=False)

			if 'stop_times' in unpacking_files:
				logger.info('Парсинг stop_times')
				gtfs_static_parser.parse_stop_times(table=config.tables[config.stops])

			if 'shapes' in unpacking_files:
				logger.info('Парсинг shapes')
				gtfs_static_parser.parse_shapes(table=config.tables[config.shapes], is_create_csv=False)

			db.query_execute(queries.create_shapes_geometry_index)
			db.query_execute(queries.create_stops_info_geometry_index)

			rmtree(dir_to_unpack)  # Удаление директории с файлами

			archive_download_ts = time()  # Обновление даты загрузки архива

		else:
			logger.error(f'Архив {zip_name} не удалось загрузить с сайта')

	# Если архив ранее не загружался, то не имеет смысла парсить информацию из бинарных файлов,
	# так как обновлять всё равно нечего. Также realtime-данные не следует обновлять, если не
	# указана переменная окружения TRANSIT_UPDATE_REALTIME, или её значение false
	if archive_download_ts is not None:
		if update_realtime:
			logger.info('Загрузка бинарных файлов с данными в реальном времени с сайта')

			# Список с названиями бинарных файлов
			file_names = [file.split('/')[-1].split('?')[0] for file in config.FILES_URLS]
			downloaded_files = []  # Загруженные файлы

			# Загрузка бинарных файлов с сайта
			for file_url, file_name in zip(config.FILES_URLS, file_names):
				if file_name == 'vehicletrips':
					ids = db.query_execute(query=queries.get_all_vehicle_ids, fetchall=True)
					query_length_limit = 6_500
					current_len = 0

					links_list = [[]]
					i = 0

					for id in ids:
						id_ = str(id[0])

						if current_len < query_length_limit:
							links_list[i].append(id_)
							current_len += len(id_)
						else:
							links_list.append([])
							i += 1
							current_len = 0

					for number, ids in enumerate(links_list, 1):
						ids_string = ','.join(ids)
						result_url = config.BASE_URL + file_url + ids_string
						filename = f'vehicletrips_{number}'

						download_file_with_print_result(file_name=filename, file_url=result_url,
														downloaded_files=downloaded_files)
				else:
					result_url = config.BASE_URL + file_url
					download_file_with_print_result(file_name=file_name, file_url=result_url,
													downloaded_files=downloaded_files)

			if downloaded_files:
				gtfs_realtime_parser = GTFSRealtimeParser()

				logger.info('Парсинг бинарных файлов с данными в реальном времени')
				for file in downloaded_files:
					gtfs_realtime_parser.define_file(file)

				db.query_execute(queries.create_vehicle_geometry_index)
			else:
				logger.error('Не удалось загрузить ни одного бинарного файла с данными в реальном времени')
	else:
		logger.error(f'В БД нет данных, поэтому обновлять нечего')


if __name__ == '__main__':
	is_need_realtime_update = os.environ.get('TRANSIT_UPDATE_REALTIME')

	# Если обновлять realtime-данные не нужно
	if is_need_realtime_update != 'true':
		delay = config.SECONDS_IN_DAY + 1 # Задержка в один день + 1 секунда
		update_realtime = False
	else:
		delay = os.environ.get('UPDATE_INTERVAL', 15)  # Переменная окружения с интервалом
		delay = float(delay)  # Преведение переменной окружения к типу с плавающей точкой

	while True:
		try:
			start_time = time()
			main()
			logger.info(f'Функция работала: {round((time() - start_time), 2)} сек.')
			sleep(delay)
		except BaseException as e:
			logger.error(f'Exeption occurred: {e}')
			db.connection_close()
			break

